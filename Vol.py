import math
def vol_esfera(Radio):
    """Instituto tecnologico de Costa Rica
Ingenieria en Computadores
Profesor: Milton Villegas Lemus
Programa: vol_esfera(Radio)
Phyton 3.6.3
Autores: Maria Jose Zamora Vargas 2018026607, Wajib Zaglul Chinchilla 2018099304
Version del programa: 1.0.0
Fecha de ultima modificacion: 26/4/2018
Entradas: Radio
Restricciones: Radio > 0, int
Salidas: Volumen"""
    Volumen = 4/3*math.pi*pow(Radio,3) #coment1
    return Volumen

def vol_piramide(Lado1,Lado2,Altura):
    """Instituto tecnologico de Costa Rica
Ingenieria en Computadores
Profesor: Milton Villegas Lemus
Programa: vol_piramide(Lado1,Lado2,Altura)
Phyton 3.6.3
Autores: Maria Jose Zamora Vargas 2018026607, Wajib Zaglul Chinchilla 2018099304
Version del programa: 1.0.0
Fecha de ultima modificacion: 26/4/2018
Entradas: Lado1,Lado2,Altura
Restricciones: Lado1>0 Lado2>0 Altura > 0, int
Salidas: Volumen"""
    Volumen=(Lado1*Lado2*Altura)/3 #coment2
    return Volumen
def area_esfera(Radio):
    """	Tecnológico de Costa Rica
	Ing. Computadores
	Profesor: Milton Villegas Lemus
	Maria José Zamora Vargas 2018026607 y Wajib Zaglul 2018099304
	Calculador de áreas
	Python 3.6.4
	Versión 1.0
	Fecha: 26/4/2018
	entradas: Radio
	restricciones: los valores de radio deben ser enteros mayores a 0
	Salidas: Área de la esfera"""
    Area=4*pi*Radio**2
    return Area#Modificación

def area_piramide(Ancho,Largo,Altura):
    """	Tecnológico de Costa Rica
	Ing. Computadores
	Profesor: Milton Villegas Lemus
	Maria José Zamora Vargas 2018026607 y Wajib Zaglul 2018099304
	Calculador de áreas
	Python 3.6.4
	Versión 1.0
	Fecha: 26/4/2018
	entradas: Ancho, Largo y Altura de la pirámide
	restricciones: los valores deben ser enteros mayores a 0
	Salidas: Área de la Pirámide"""
    Area= (Ancho*Largo)+((Ancho*2+Largo*2)*Altura)/2
    return Area #Modificación
def area_cilindro(Radio,Altura):
    Area=2*pi*Radio*(Altura+Radio)
